const { client } = require('./../config/clientDefine.js');  // importing client from clientDrfine.js


function highestRevenue(req, res){ // query to get the result
    return client.query(`select Id, Title, Revenue_million from movies_description order by Revenue_million desc limit 1`);
    
}

function leastRevenue(){  // query to get the result
    return client.query(`select Id, Title, Revenue_million from movies_description order by Revenue_million limit 1`);
}

function insertDirector(body){ // query to get the result
    return client.query(`insert into movies_description(id,title,genre,description,year,runtime_mint,revenue_million ) values(${body.id},'${body.title}','${body.genre}','${body.description}',${body.year},${body.runtime_mint},${body.revenue_million})`)
}

function deleteMovieDescription(body){ // query to get the result
    return client.query(`delete from movies_description where Id = ${body.id}`);
}


module.exports = {          // exporting function
    highestRevenue,
    leastRevenue,
    insertDirector,
    deleteMovieDescription
}