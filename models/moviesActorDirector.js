const { client } = require('./../config/clientDefine.js');  // importing client from clientDrfine.js


function moviesDirectedBy() // query function to get the result
{
    return client.query(`select d.id, d.title from movies_description d where exists(select a.id from movies_actordirector a where d.id = a.id and a.director ='James Gunn')`);
}

function highestMovieMadebyAdirector(){ // query function to get the result
    return client.query(`select director, count(*) from movies_actordirector group by director order by count desc limit 1`);
}

module.exports = {        // exporting function
    moviesDirectedBy,
    highestMovieMadebyAdirector
}