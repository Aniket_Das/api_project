const { client } = require('./../config/clientDefine.js');  // importing client from clientDrfine.js

function topRatedMovies(){ // query to get the result
    return client.query(`select d.Id, d.Title from movies_description d INNER JOIN movies_rank r ON r.Id = d.Id order by Rating desc limit 10 `);
}

function top10Metscore(){  // query to get the result
    return client.query(`select d.Id, d.Title from movies_description d INNER JOIN r.metascore movies_rank r ON r.Id = d.Id order by metascore desc limit 10`);
}


module.exports = {      // exporting function
    top10Metscore,
    topRatedMovies
}