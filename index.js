const express = require('express');     //import express
const app = express();

const {client} = require('./config/clientDefine.js');     // importing clientDefine.js
client.connect();

const moviesdescriptionRoutes = require('./routes/moviesDescriptionRoutes.js'); // importing moviesdescriptionRoutes
const acotrdirectorRoutes = require('./routes/moviesActorDirectorRoutes.js'); // importing actordirectorRoutes
const moviesrantingRoutes = require('./routes/moviesRatingRoutes.js'); // importmoviesrantingRoutes

app.use('/movies', moviesdescriptionRoutes);        // if app.use found in url '/movies' it will execute only moviesdrscriptioRoutes
app.use('/movieactordirector', acotrdirectorRoutes);    // if app.use found in url '/movies' it will execute only actordirectorRoutes
app.use('/movieranking', moviesrantingRoutes);      // if app.use found in url '/movies' it will execute only moviesratingRoutes


const port = process.env.PORT || 6000; // using localhost port 6000 or any port from enviroment


app.listen(port, () => console.log(`Listening on port ${port}...`));    //The app.listen() function creates the Node.js web server at the specified host and port. It is identical to Node's http.Server.listen() method.  


