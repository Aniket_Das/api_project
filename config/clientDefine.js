const { Client } = require('pg')
const client = new Client({
    user: 'postgres',
    password: 'test1234',
    host: 'localhost',
    port: 5432,
    database: 'api_project_db',
});

module.exports = {
    client
}