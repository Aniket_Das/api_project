const express = require('express'); // impport express
const router = express.Router();    // using router module of express

const moviesDescription = require('./../middleware/moviesDescription.js'); // importing the moviesActor.js 



// router.get('/', (req, res, next) => {
//     res.status(200).json({
//         message : 'Handeling GET, POST, DELETE request to /movies(2006-2016)'
//     });
// });


router.get('/query_havingTopRevenue', moviesDescription.havingTopRevenue); // execute get request

router.get('/query_leastRevenue', moviesDescription.leastRevenue); // execute get request

router.post('/query_insertdirector', moviesDescription.insertdetails); // execute get request

router.delete('/query_deleteMovie',moviesDescription.deleteMovie); // execute get request

module.exports = router;  //export router