const express = require('express');     //import express    
const router = express.Router();        //using router module of express

const moviesRanking = require('../middleware/moviesRating.js'); // importing the moviesActor.js 

// router.get('/', (req, res, next) => {
//     res.status(200).json({
//         message : 'Handeling GET, POST, DELETE request to /rating(2006-2016)'
//     });
// });

router.get('/topratedmovies',moviesRanking.topratedmovies);     //execute get request

router.get('/top10metascore',moviesRanking.top10metascore);     //execute get request

module.exports = router;        // export router