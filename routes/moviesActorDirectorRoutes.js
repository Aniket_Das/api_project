const express = require('express');  // importing express 
const router = express.Router();       // using router module of express

const moviesActorDirector = require('./../middleware/moviesActor.js'); // importing the moviesActor.js 

// router.get('/', (req, res, next) => {
//     res.status(200).json({
//         message : 'Handeling GET, POST, DELETE request to /rating(2006-2016)'
//     });
// });

router.get('/moviedirectedbyJames_Gunn',moviesActorDirector.moviesdirectedby);  // will execute the get request

router.get('/highestmoviemadebydirector',moviesActorDirector.highestmoviemadeby); // will execute the get request

module.exports = router; // exporting router