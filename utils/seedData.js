const { client } = require('./../config/clientDefine.js'); // importing clientDefine.js

async function table_creation(tableName, secondTableName, thirdTableName) {     // function to create table
    await client.connect();

    await client.query(`DROP TABLE IF EXISTS ${tableName} CASCADE`);
    await client.query(`create table ${tableName}(Id int , Title char(100),Genre char(100),Description char(500),Year int,Runtime_mint float,Revenue_million float)`);
    await client.query(`COPY ${tableName} FROM '/mnt/c/Users/anike/API_Project/src/data/IMDB_movies_description.csv' DELIMITER ',' CSV HEADER`);
    await client.query(`DROP TABLE IF EXISTS ${secondTableName} CASCADE`);
    await client.query(`create table ${secondTableName}(Id int ,Director varchar(500),Actors varchar(500))`);
    // client.query(`ALTER TABLE ${secondTableName} ADD FOREIGN KEY (Id) REFERENCES ${tableName}(Id)`);
    await client.query(`COPY ${secondTableName} FROM '/mnt/c/Users/anike/API_Project/src/data/IMDB_movies_actor_director.csv' DELIMITER ',' CSV HEADER`);
    await client.query(`DROP TABLE IF EXISTS ${thirdTableName} CASCADE`);
    await client.query(`CREATE TABLE ${thirdTableName}(Id int ,Rating float,Votes int,Metascore int)`);
    // client.query(`ALTER TABLE ${thirdTableName} ADD FOREIGN KEY (Id) REFERENCES ${tableName}(Id)`)
    await client.query(`COPY ${thirdTableName} FROM '/mnt/c/Users/anike/API_Project/src/data/IMDB_movies_rating.csv' DELIMITER ',' CSV HEADER`);
    
    await client.end();

}
table_creation('movies_description', 'movies_actorDirector', 'movies_rank');       //calling the function table_creation
