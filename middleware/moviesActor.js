

const moviesActorDirector = require('./../models/moviesActorDirector.js'); // importing moviesActorDirector from models

function moviesdirectedby(req, res){
    moviesActorDirector.moviesDirectedBy().then(data => res.send(data.rows)).catch(err => console.log(err)) // using moviesActorDirector calling the query from model
}

function highestmoviemadeby(req, res){
    moviesActorDirector.highestMovieMadebyAdirector().then(data => res.send(data.rows)).catch(err => console.log(err)) // using moviesActorDirector calling the query from model
}


module.exports ={       // exporting function
    moviesdirectedby,
    highestmoviemadeby
}