
const moviesRanking = require('./../models/moviesRating.js');  // importing moviesRating from models

function topratedmovies(req, res){
    moviesRanking.topRatedMovies().then(data => res.send(data.rows)).catch(err => console.log(err))    // using moviesRating object to call query from model
}

function top10metascore(req, res){
    moviesRanking.top10Metscore().then(data => res.send(data.rows)).catch(err => console.log(err))    // using moviesRating object to call query from model
}


module.exports ={       // exporting function
    topratedmovies,
    top10metascore
}