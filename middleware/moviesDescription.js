const moviesDescription = require('./../models/moviesDescription.js'); // importing moviesDescription from model

function havingTopRevenue(req, res){
    moviesDescription.highestRevenue().then(data => res.send(data.rows)).catch(err => console.log(err))  // using moveisDescription calling the query from model
}

function leastRevenue(req, res){
    moviesDescription.leastRevenue().then(data => res.send(data.rows)).catch(err => console.log(err))   // using moveisDescription calling the query from model
}

function insertdetails(req, res){
    moviesDescription.insertDirector(req.body).then(data => res.send(data.rows)).catch(err => console.log(err))  // using moveisDescription calling the query from model
}

function deleteMovie(req, res){
    moviesDescription.deleteMovieDescription(req.body).then(data => res.send(data.rows)).catch(err => console.log(err))  // using moveisDescription calling the query from model
}

module.exports ={       // exporting the function
    havingTopRevenue,
    leastRevenue,
    insertdetails,
    deleteMovie
}